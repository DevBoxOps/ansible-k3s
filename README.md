# k3s

Role for install k3s.

## Deploy example

```yaml
  roles:
    - role: k3s
      k3s_token: "something_token"
      k3s_datastore_endpoint: postgres://k3s:pass@postgres:5432/k3s
```

## Available parameters

### Main

| Param | Default | Description |
| -------- | -------- | -------- |
| `k3s_binary_dir` | `/usr/local/bin` | Dir to k3s binary |
| `k3s_binary_name` | `k3s` | k3s binary name |
| `k3s_force_update` | `false` | K3s force update |
| `k3s_cluster_cidr` | `10.42.0.0/16` | K3S cluster cidr |
| `k3s_service_cidr` | `10.42.0.0/16` | K3S service cidr |
| `k3s_datastore_endpoint` | `""` | K3S datastore endpoint |
| `k3s_token` | `""` | K3S token |
| `k3s_kubeconfig_mode` | `600` | K3S kubeconfig mode |
| `k3s_version` | `v1.28.3+k3s2` | K3S version |

## TODO

1. Added support install worker node
